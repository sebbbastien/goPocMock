package mypackage

import (
	"net"
)

func GetIfaceByName(ifaceName string) (iface *net.Interface, err error) {
	iface, err = net.InterfaceByName(ifaceName)
	if err != nil {
		return nil, err
	}
	return iface, nil
}
