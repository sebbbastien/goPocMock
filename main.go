package main

import (
	"net"

	"github.com/astaxie/beego/logs"
	"gitlab.com/sebbbastien/goPocMock/mypackage"
	"gitlab.com/sebbbastien/goPocMock/mypackage2"
)

func main() {
	var i *net.Interface
	var e error

	// ***** mypackage *****
	// appel normal, cette interface existe sur mon PC
	logs.Notice("===== Cas 1 =====")
	i, e = mypackage.GetIfaceByName("wlp2s0")
	if e != nil {
		logs.Error(e)
	}
	logs.Debug(i)

	// ***** mypackage2 *****
	// appel pas normal, cette interface n'existe sur mon PC, on veut une erreur
	logs.Notice("===== Cas 2 =====")
	i, e = mypackage2.GetIfaceByName("eth0")
	if e != nil {
		logs.Error(e)
	}
	logs.Debug(i)

	// on dit à la lib d'activer la fonction de mock
	mypackage2.EnableMock()

	// on fait le même appel sur eth0 qui n'existe pas
	logs.Notice("===== Cas 3 =====")
	i, e = mypackage2.GetIfaceByName("eth0")
	if e != nil {
		logs.Error(e)
	}
	logs.Debug(i)

	// On peut même désactiver les fonctions mockées
	mypackage2.DisableMock()

	// on fait le même appel sur eth0 qui n'existe pas, et on a à nouveau une erreur
	logs.Notice("===== Cas 4 =====")
	i, e = mypackage2.GetIfaceByName("eth0")
	if e != nil {
		logs.Error(e)
	}
	logs.Debug(i)

}
