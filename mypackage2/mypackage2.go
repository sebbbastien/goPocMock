package mypackage2

import (
	"net"
)

// Défini le profil de la fonction à mocker
type getIfaceByNameFuncType = func(string) (*net.Interface, error)

// La fonction qui sera utilisée lors de l'appel de GetIfaceByName()
var getIfaceByNameFunc getIfaceByNameFuncType = getIfaceByName

// La fonction qui va être appellée et qui va faire le choix d'appeller
// la vraie fonction ou bien celle de mock
func GetIfaceByName(ifaceName string) (*net.Interface, error) {
	return getIfaceByNameFunc(ifaceName)
}

// La fonction telle qu'elle était sans le mock
func getIfaceByName(ifaceName string) (iface *net.Interface, err error) {
	iface, err = net.InterfaceByName(ifaceName)
	if err != nil {
		return nil, err
	}
	return iface, nil
}

func EnableMock() {
	// On change la fonction qui devra être exécutée lors de l'appel de getIfaceByNameFunc()
	getIfaceByNameFunc = func(ifaceName string) (*net.Interface, error) {
		i := net.Interface{
			Index:        99,
			MTU:          9999,
			Name:         ifaceName,
			HardwareAddr: net.HardwareAddr{0x99, 0x99, 0x99, 0x99, 0x99, 0x99},
			Flags:        net.FlagUp | net.FlagBroadcast,
		}

		return &i, nil
	}
}

func DisableMock() {
	getIfaceByNameFunc = getIfaceByName
}
